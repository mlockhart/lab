# Pet GitLab

This is my pet GitLab. Its purpose is to be fed and maintained like a pet, and updated regularly. I may conduct tests and experiments on it, but it will always be current, or nearly so.

It's based upon openSUSE, because I like that distro.

Custom configurations will be made in an ad-hoc fashion at first, but I may eventually move to provisioning via my personal [Radix](htps://gitlab.com/milohax-net/radix/) group of projects.
