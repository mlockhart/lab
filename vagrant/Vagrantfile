# -*- mode: ruby -*-
# vi: set ft=ruby :

vm_name = "lab"
vm_hostname=vm_name + ".vagrant"
vm_url = "http://" + vm_hostname
vm_host_port = 8888

Vagrant.configure("2") do |config|
  config.vm.define vm_name
  config.vm.box = "opensuse/Leap-15.3.x86_64"

  # Don't auto-update the Virtualbox Guest Editions -- breaks the box
  config.vbguest.auto_update = false
  config.vm.box_check_update = false

  # NOTE: This will enable public access to the opened port
  config.vm.network :forwarded_port, guest: 80, host: vm_host_port
  config.vm.hostname = vm_hostname

  config.vm.provider "virtualbox" do |vb|
  # https://docs.gitlab.com/ee/install/requirements.html#hardware-requirements
  # Recommended minimum is 4 cores, 4GiB RAM + 2GiB swap, to support 500 users
      vb.memory = "6144"
      vb.cpus = 4
      vb.name = vm_name
  end

  # Provision GitLab with a shell script, based upon instructions at
  # https://about.gitlab.com/install/#opensuse-leap-15-2-15-3
  config.vm.provision "shell", inline: <<-SHELL
    zypper install -y curl openssh perl hostname
    # Enable OpenSSH server daemon if not enabled:
    systemctl enable sshd
    systemctl start sshd
    # Install and enable firewall daemon:
    zypper install -y firewalld
    systemctl enable firewalld
    systemctl start firewalld
    firewall-cmd --permanent --add-service=http --zone=public
    firewall-cmd --permanent --add-service=https --zone=public
    systemctl reload firewalld
    # Install and enable postfix daemon:
    zypper install -y postfix
    systemctl enable postfix
    systemctl start postfix

    # Add GitLab package repository
    curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | bash

    # INSTALL GITLAB
    EXTERNAL_URL=#{ vm_hostname } zypper install -y gitlab-ee
    PWD_FILE=/etc/gitlab/initial_root_password
    test -e ${PWD_FILE} && echo "INITIAL PASSWORD: $(awk '/^Password:/ {print $2}' ${PWD_FILE})"
    echo "GITLAB URL: #{ vm_url }:#{ vm_host_port }"
  SHELL
end
