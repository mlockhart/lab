# GITLAB variables are set in dotfiles .config/env/gitlab.env
string match --quiet (hostname) "$GITLAB_WORKSTATION"
  and begin
    fish_add_path --prepend \
      $GITLAB_LAB_BIN \
      ~/Library/Application\ Support/cloud-code/installer/google-cloud-sdk/bin/ \
      ~/lab/gitlab.com/gitlab-com/support/toolbox/dotfiles/bin
  end

