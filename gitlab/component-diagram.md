### Component diagram

The Single Source of Truth for this is always the [Gitlab architecture documentation](https://docs.gitlab.com/ee/development/architecture.html#component-diagram), but to see the entire diagram you need to zoom to about 30%, making it unreadable. Without the page overview, you can view the diagram on _this_ page at about 50%, which is a bit better.  Best would be to print it out on a large sheet of paper.

```mermaid
%%{init: {"flowchart": { "useMaxWidth": false } }}%%
graph LR
  %% Anchor items in the appropriate subgraph.
  %% Link them where the destination* is.

  subgraph Clients
    Browser((Browser))
    Git((Git))
  end

  %% External Components / Applications
  Geo{{GitLab Geo}} -- TCP 80, 443 --> HTTP
  Geo -- TCP 22 --> SSH
  Geo -- TCP 5432 --> PostgreSQL
  Runner{{GitLab Runner}} -- TCP 443 --> HTTP
  K8sAgent{{GitLab Agent}} -- TCP 443 --> HTTP

  %% GitLab Application Suite
  subgraph GitLab
    subgraph Ingress
        HTTP[[HTTP/HTTPS]]
        SSH[[SSH]]
        NGINX[NGINX]
        GitLabShell[GitLab Shell]

        %% inbound/internal
        Browser -- TCP 80,443 --> HTTP
        Git -- TCP 80,443 --> HTTP
        Git -- TCP 22 --> SSH
        HTTP -- TCP 80, 443 --> NGINX
        SSH -- TCP 22 --> GitLabShell
    end

    subgraph GitLab Services
        %% inbound from NGINX
        NGINX --> GitLabWorkhorse
        NGINX -- TCP 8090 --> GitLabPages
        NGINX -- TCP 8150 --> GitLabKas
        NGINX --> Registry
        %% inbound from GitLabShell
        GitLabShell --> GitLabWorkhorse

        %% services
        Puma["Puma (GitLab Rails)"]
        Puma <--> Registry
        GitLabWorkhorse[GitLab Workhorse] <--> Puma
        GitLabKas[GitLab Agent Server] --> GitLabWorkhorse
        GitLabPages[GitLab Pages] --> GitLabWorkhorse
        Mailroom
        Sidekiq
    end

    subgraph Integrated Services
        %% Mattermost
        Mattermost
        Mattermost ---> GitLabWorkhorse
        NGINX --> Mattermost

        %% Grafana
        Grafana
        NGINX --> Grafana
    end

    subgraph Metadata
        %% PostgreSQL
        PostgreSQL
        PostgreSQL --> Consul

        %% Consul and inbound
        Consul
        Puma ---> Consul
        Sidekiq ---> Consul
        Migrations --> PostgreSQL

        %% PgBouncer and inbound
        PgBouncer
        PgBouncer --> Consul
        PgBouncer --> PostgreSQL
        Sidekiq --> PgBouncer
        Puma --> PgBouncer
    end

    subgraph State
        %% Redis and inbound
        Redis
        Puma --> Redis
        Sidekiq --> Redis
        GitLabWorkhorse --> Redis
        Mailroom --> Redis
        GitLabKas --> Redis

        %% Sentinel and inbound
        Sentinel <--> Redis
        Puma --> Sentinel
        Sidekiq --> Sentinel
        GitLabWorkhorse --> Sentinel
        Mailroom --> Sentinel
        GitLabKas --> Sentinel
    end

    subgraph Git Repositories
        %% Gitaly / Praefect
        Praefect --> Gitaly
        GitLabKas --> Praefect
        GitLabShell --> Praefect
        GitLabWorkhorse --> Praefect
        Puma --> Praefect
        Sidekiq --> Praefect
        Praefect <--> PraefectPGSQL[PostgreSQL]
        %% Gitaly makes API calls
        %% Ordered here to ensure placement.
        Gitaly --> GitLabWorkhorse
    end

    subgraph Storage
        %% ObjectStorage and inbound traffic
        ObjectStorage["Object Storage"]
        Puma -- TCP 443 --> ObjectStorage
        Sidekiq -- TCP 443 --> ObjectStorage
        GitLabWorkhorse -- TCP 443 --> ObjectStorage
        Registry -- TCP 443 --> ObjectStorage
        GitLabPages -- TCP 443 --> ObjectStorage
    end

    subgraph Monitoring
        %% Prometheus
        Grafana -- TCP 9090 --> Prometheus[Prometheus]
        Prometheus -- TCP 80, 443 --> Puma
        RedisExporter[Redis Exporter] --> Redis
        Prometheus -- TCP 9121 --> RedisExporter
        PostgreSQLExporter[PostgreSQL Exporter] --> PostgreSQL
        PgBouncerExporter[PgBouncer Exporter] --> PgBouncer
        Prometheus -- TCP 9187 --> PostgreSQLExporter
        Prometheus -- TCP 9100 --> NodeExporter[Node Exporter]
        Prometheus -- TCP 9168 --> GitLabExporter[GitLab Exporter]
        Prometheus -- TCP 9127 --> PgBouncerExporter
        Prometheus --> Alertmanager
        GitLabExporter --> PostgreSQL
        GitLabExporter --> GitLabShell
        GitLabExporter --> Sidekiq

        %% Alertmanager
        Alertmanager -- TCP 25 --> SMTP
    end
  %% end subgraph GitLab
  end

  subgraph External
    subgraph External Services
        SMTP[SMTP Gateway]
        LDAP

        %% Outbound SMTP
        Sidekiq -- TCP 25 --> SMTP
        Puma -- TCP 25 --> SMTP
        Mailroom -- TCP 25 --> SMTP

        %% Outbound LDAP
        Puma -- TCP 369 --> LDAP
        Sidekiq -- TCP 369 --> LDAP

        %% Elasticsearch
        Elasticsearch
        Puma -- TCP 9200 --> Elasticsearch
        Sidekiq -- TCP 9200 --> Elasticsearch
    end
    subgraph External Monitoring
        %% Sentry
        Sidekiq -- TCP 80, 443 --> Sentry
        Puma -- TCP 80, 443 --> Sentry

        %% Jaeger
        Jaeger
        Sidekiq -- UDP 6831 --> Jaeger
        Puma -- UDP 6831 --> Jaeger
        Gitaly -- UDP 6831 --> Jaeger
        GitLabShell -- UDP 6831 --> Jaeger
        GitLabWorkhorse -- UDP 6831 --> Jaeger
    end
  %% end subgraph External
  end

click Alertmanager "https://docs.gitlab.com/ee/development/architecture.html#alertmanager"
click Praefect "https://docs.gitlab.com/ee/development/architecture.html#praefect"
click Geo "https://docs.gitlab.com/ee/development/architecture.html#gitlab-geo"
click NGINX "https://docs.gitlab.com/ee/development/architecture.html#nginx"
click Runner "https://docs.gitlab.com/ee/development/architecture.html#gitlab-runner"
click Registry "https://docs.gitlab.com/ee/development/architecture.html#registry"
click ObjectStorage "https://docs.gitlab.com/ee/development/architecture.html#minio"
click Mattermost "https://docs.gitlab.com/ee/development/architecture.html#mattermost"
click Gitaly "https://docs.gitlab.com/ee/development/architecture.html#gitaly"
click Jaeger "https://docs.gitlab.com/ee/development/architecture.html#jaeger"
click GitLabWorkhorse "https://docs.gitlab.com/ee/development/architecture.html#gitlab-workhorse"
click LDAP "https://docs.gitlab.com/ee/development/architecture.html#ldap-authentication"
click Puma "https://docs.gitlab.com/ee/development/architecture.html#puma"
click GitLabShell "https://docs.gitlab.com/ee/development/architecture.html#gitlab-shell"
click SSH "https://docs.gitlab.com/ee/development/architecture.html#ssh-request-22"
click Sidekiq "https://docs.gitlab.com/ee/development/architecture.html#sidekiq"
click Sentry "https://docs.gitlab.com/ee/development/architecture.html#sentry"
click GitLabExporter "https://docs.gitlab.com/ee/development/architecture.html#gitlab-exporter"
click Elasticsearch "https://docs.gitlab.com/ee/development/architecture.html#elasticsearch"
click Migrations "https://docs.gitlab.com/ee/development/architecture.html#database-migrations"
click PostgreSQL "https://docs.gitlab.com/ee/development/architecture.html#postgresql"
click Consul "https://docs.gitlab.com/ee/development/architecture.html#consul"
click PgBouncer "https://docs.gitlab.com/ee/development/architecture.html#pgbouncer"
click PgBouncerExporter "https://docs.gitlab.com/ee/development/architecture.html#pgbouncer-exporter"
click RedisExporter "https://docs.gitlab.com/ee/development/architecture.html#redis-exporter"
click Redis "https://docs.gitlab.com/ee/development/architecture.html#redis"
click Prometheus "https://docs.gitlab.com/ee/development/architecture.html#prometheus"
click Grafana "https://docs.gitlab.com/ee/development/architecture.html#grafana"
click GitLabPages "https://docs.gitlab.com/ee/development/architecture.html#gitlab-pages"
click PostgreSQLExporter "https://docs.gitlab.com/ee/development/architecture.html#postgresql-exporter"
click SMTP "https://docs.gitlab.com/ee/development/architecture.html#outbound-email"
click NodeExporter "https://docs.gitlab.com/ee/development/architecture.html#node-exporter"
```

### Component legend

- ✅ - Installed by default
- ⚙ - Requires additional configuration
- ⤓ - Manual installation required
- ❌ - Not supported or no instructions available
- N/A - Not applicable

Component statuses are linked to configuration documentation for each component.

### Component list

| Component                                             | Description                                                          | [Omnibus GitLab](https://docs.gitlab.com/omnibus/) | [GitLab Environment Toolkit (GET)](https://gitlab.com/gitlab-org/gitlab-environment-toolkit) | [GitLab chart](https://docs.gitlab.com/charts/) | [minikube Minimal](https://docs.gitlab.com/charts/development/minikube/#deploying-gitlab-with-minimal-settings) | [GitLab.com](https://gitlab.com) | [Source](https://docs.gitlab.com/ee/install/installation.html)| [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) |  [CE/EE](https://about.gitlab.com/install/ce-or-ee/)  |
|-------------------------------------------------------|----------------------------------------------------------------------|:--------------:|:--------------:|:------------:|:----------------:|:----------:|:------:|:---:|:-------:|
| [Certificate Management](#certificate-management)     | TLS Settings, Let's Encrypt                                          |       ✅       |       ✅        |      ✅       |        ⚙         |     ✅      |   ⚙    |  ⚙  | CE & EE |
| [Consul](#consul)                                     | Database node discovery, failover                                    |       ⚙       |       ✅         |      ❌       |        ❌         |     ✅      |   ❌    |  ❌  | EE Only |
| [Database Migrations](#database-migrations)           | Database migrations                                                  |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ⚙    |  ✅  | CE & EE |
| [Elasticsearch](#elasticsearch)                       | Improved search within GitLab                                        |       ⤓        |       ⚙        |      ⤓       |        ⤓         |     ✅      |   ⤓    |  ⚙ | EE Only |
| [Gitaly](#gitaly)                                     | Git RPC service for handling all Git calls made by GitLab            |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ⚙    |  ✅  | CE & EE |
| [GitLab Exporter](#gitlab-exporter)                   | Generates a variety of GitLab metrics                                |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ❌    |  ❌  | CE & EE |
| [GitLab Geo](#gitlab-geo)                        | Geographically distributed GitLab site                              |       ⚙        |       ⚙      |        ❌      |        ❌         |     ✅      |   ❌    |  ⚙  | EE Only |
| [GitLab Pages](#gitlab-pages)                         | Hosts static websites                                                |       ⚙       |       ⚙        |      ❌       |        ❌         |     ✅      |   ⚙    |  ⚙  | CE & EE |
| [GitLab agent](#gitlab-agent)              | Integrate Kubernetes clusters in a cloud-native way                  |       ⚙       |       ⚙        |      ⚙       |        ❌         |     ❌      |   ⤓    |  ⚙   | EE Only |
| [GitLab self-monitoring: Alertmanager](#alertmanager) | Deduplicates, groups, and routes alerts from Prometheus              |       ⚙       |       ⚙        |      ✅       |        ⚙         |     ✅      |   ❌    |  ❌  | CE & EE |
| [GitLab self-monitoring: Grafana](#grafana)           | Metrics dashboard                                                    |       ✅       |       ✅        |      ⚙       |        ⤓         |     ✅      |   ❌    |  ⚙  | CE & EE |
| [GitLab self-monitoring: Jaeger](#jaeger)             | View traces generated by the GitLab instance                         |       ❌       |       ⚙        |      ⚙       |        ❌         |     ❌      |   ⤓    |  ⚙  | CE & EE |
| [GitLab self-monitoring: Prometheus](#prometheus)     | Time-series database, metrics collection, and query service          |       ✅       |       ✅        |      ✅       |        ⚙         |     ✅      |   ❌    |  ⚙  | CE & EE |
| [GitLab self-monitoring: Sentry](#sentry)             | Track errors generated by the GitLab instance                        |       ⤓        |       ⤓        |      ⤓       |        ❌         |     ✅      |   ⤓    |  ⤓  | CE & EE |
| [GitLab Shell](#gitlab-shell)                         | Handles `git` over SSH sessions                                      |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ⚙    |  ✅  | CE & EE |
| [GitLab Workhorse](#gitlab-workhorse)                 | Smart reverse proxy, handles large HTTP requests                     |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ⚙    |  ✅  | CE & EE |
| [Inbound email (SMTP)](#inbound-email)                | Receive messages to update issues                                    |       ⤓        |       ⤓        |      ⚙       |        ⤓         |     ✅      |   ⤓    |  ⤓  | CE & EE |
| [Jaeger integration](#jaeger)                         | Distributed tracing for deployed apps                                |       ⤓        |       ⤓        |      ⤓       |        ⤓         |     ⤓      |   ⤓    |  ⚙  | EE Only |
| [LDAP Authentication](#ldap-authentication)           | Authenticate users against centralized LDAP directory                |       ⤓        |       ⤓        |      ⤓       |        ⤓         |     ❌      |   ⤓    |  ⚙  | CE & EE |
| [Mattermost](#mattermost)                             | Open-source Slack alternative                                        |       ⚙       |       ⚙        |      ⤓       |        ⤓         |     ⤓      |   ❌    |  ⚙  | CE & EE |
| [MinIO](#minio)                                       | Object storage service                                               |       ⤓        |       ⤓        |      ✅       |        ✅         |     ✅      |   ❌    |  ⚙  | CE & EE |
| [NGINX](#nginx)                                       | Routes requests to appropriate components, terminates SSL            |       ✅       |       ✅        |      ✅       |        ⚙         |     ✅      |   ⤓    |  ⚙  | CE & EE |
| [Node Exporter](#node-exporter)                       | Prometheus endpoint with system metrics                              |       ✅       |       ✅        |     N/A      |       N/A        |     ✅      |   ❌    |  ❌  | CE & EE |
| [Outbound email (SMTP)](#outbound-email)              | Send email messages to users                                         |       ⤓        |       ⤓        |      ⚙       |        ⤓         |     ✅      |   ⤓    |  ⤓  | CE & EE |
| [Patroni](#patroni)                                   | Manage PostgreSQL HA cluster leader selection and replication        |       ⚙       |       ✅        |      ❌       |        ❌         |     ✅      |   ❌    |  ❌  | EE Only |
| [PgBouncer Exporter](#pgbouncer-exporter)             | Prometheus endpoint with PgBouncer metrics                           |       ⚙       |       ✅        |      ❌       |        ❌         |     ✅      |   ❌    |  ❌  | CE & EE |
| [PgBouncer](#pgbouncer)                               | Database connection pooling, failover                                |       ⚙       |       ✅        |      ❌       |        ❌         |     ✅      |   ❌    |  ❌  | EE Only |
| [PostgreSQL Exporter](#postgresql-exporter)           | Prometheus endpoint with PostgreSQL metrics                          |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ❌    |  ❌  | CE & EE |
| [PostgreSQL](#postgresql)                             | Database                                                             |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ⤓    |  ✅  | CE & EE |
| [Praefect](#praefect)                                 | A transparent proxy between any Git client and Gitaly storage nodes. |       ✅       |       ✅        |      ⚙       |        ❌         |     ✅      |   ⚙    |  ✅  | CE & EE |
| [Puma (GitLab Rails)](#puma)                          | Handles requests for the web interface and API                       |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ⚙    |  ✅  | CE & EE |
| [Redis Exporter](#redis-exporter)                     | Prometheus endpoint with Redis metrics                               |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ❌    |  ❌  | CE & EE |
| [Redis](#redis)                                       | Caching service                                                      |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ⤓    |  ✅  | CE & EE |
| [Registry](#registry)                                 | Container registry, allows pushing and pulling of images             |       ⚙       |       ⚙        |      ✅       |        ✅         |     ✅      |   ⤓    |  ⚙  | CE & EE |
| [Runner](#gitlab-runner)                              | Executes GitLab CI/CD jobs                                           |       ⤓        |       ⤓        |      ✅       |        ⚙         |     ✅      |   ⚙    |  ⚙  | CE & EE |
| [Sentry integration](#sentry)                         | Error tracking for deployed apps                                     |       ⤓        |       ⤓        |      ⤓       |        ⤓         |     ⤓      |   ⤓    |  ⤓  | CE & EE |
| [Sidekiq](#sidekiq)                                   | Background jobs processor                                            |       ✅       |       ✅        |      ✅       |        ✅         |     ✅      |   ✅    |  ✅  | CE & EE |

