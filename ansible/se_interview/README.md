# Steps to build an SE interview instance

This instance will be used to conduct a [GitLab Support Engineer technical interview](https://gitlab.com/gitlab-com/support/tech-interview/se-interview).

The [documented/official way to create an instance](https://handbook.gitlab.com/handbook/support/workflows/team/technical_interview/) is with the GitLab Sandbox Cloud. I have these plays and scripts mostly as a backup, in case of trouble running pipelines on `gitlabsandbox.cloud`. It is also a good way to learn some Ansible.

1. Create the instance

    ```shell
    ansible-playbook create_interview_host.yml
    ```

1. Upload and run the builder

    ```shell
    ansible-playbook -i inventory.ini upload_and_run_builder.yml
    ```

NOTES:

- The SSH access relies on the public keys in gitlab.com. If running
  from a new Ansible controller, be sure to generate an SSH key and add
  it to gitlab.com
- The
  [SE-interview-builder](https://gitlab.com/gitlab-com/support/tech-interview/se-interview-builder)
  is used to actually prepare the VM for an interview. This is copied
  from the location of `src_interview_builder` on the Ansible
  controller. It is cloned from GitLab with SSH, and then uploaded, to
  save the need to manage access tokens on the interview host
- While Ansible is _idempotent_, the SE builder uses Chef, which is
  _not_. The chef task will only be skipped on repeated runs.

