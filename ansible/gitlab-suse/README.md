# Install single-server GitLab on openSUSE

This is a simple playbook, no special checks are performed. If you need a production-ready deployment method, use the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit).

This playbook follows the [instructions to install GitLab Omnibus in SUSE](https://about.gitlab.com/install/#opensuse-leap). It is an experimental tool for repeatedly installing GitLab automatically to servers/VMs that have been manually provisioned. Here is what I explored and learned:

1. Initially tried using the `geerlingguy.gitlab` Ansible Galaxy role but found it didn't support SUSE properly
1. Created my own playbook using the [official GitLab Omnibus package installation](https://docs.gitlab.com/omnibus/installation/)
1. Discovered several key points about GitLab installation on openSUSE:
    -   Can use `EXTERNAL_URL`, `GITLAB_ROOT_PASSWORD` and `GITLAB_ROOT_EMAIL` environment variables during installation, to avoid extra reconfigure
    -   Need to handle repository management carefully
    -   GitLab maintains RPM packages in repositories matching specific openSUSE versions
    -   For older GitLab versions, need to use repository from an older openSUSE version (e.g., 15.5 for GitLab 17.1.0, since there is no RPM package for it on openSUSE 15.6)

## Using this playbook

1. Ensure the host info in the `inventory.ini` is up to date. This may require a matching entry in `~/.ssh.d/config`
1. Review `vars.yml` variables and set according to your requirements
1. Run with:

```shell
ansible-playbook -i inventory.ini install-gitlab.yml
```
1. After the play ends successfully, [retrieve the initial root password](https://docs.gitlab.com/omnibus/installation/#set-up-the-initial-account):

```shell
cat /etc/gitlab/initial_root_password
```

## Key features

-   Installs either latest or specific versions of GitLab EE and GitLab CE
-   Configures external URL and root email/password during installation
-   Uses repository from specific openSUSE version for package availability
-   Waits for GitLab to be ready before completing

## Variables

These variable determine what is installed:

- `ansible_ssh_user`: the SSH user that Ansible uses to connect to the host
- `gitlab_external_url`: set to the URL that can reach the host
- `gitlab_edition`:  Either `"gitlab-ee"`, or `"gitlab-ce"` for community edition
- `gitlab_version`:  _Optional_ - remove to install latest GitLab. Use _major_._minor_._patch_ numbering
- `gitlab_repo_opensuse_version`: The GitLab OS-release repo that the package is in. Not necessarily the same as the server's OS version! Review [the GitLab packages repository](https://packages.gitlab.com/app/gitlab/gitlab-ee/search?q=&filter=rpms&dist=opensuse) and note the "**Distro/Version**" for the desired GitLab version. For instance 17.6.0 is available in the the opensuse/`15.6` repository, but 17.5.5 is only available in opensuse/`15.5`.  This is also announced in GitLab release notes but it's easier to search in the package repo directly.
- `gitlab_root_email`: _Optional_ - remove for `root@{{gitlab_external_domain}}`
- `gitlab_root_password`: _Optional_ - remove for usual random password

## Caveats

This goes as far as installing the package and configuring the external URL. It does _not_ configure:
  - [GitLab Container registry](https://docs.gitlab.com/ee/administration/packages/container_registry.html#container-registry-domain-configuration)
  - [Enabling GitLab Pages](https://docs.gitlab.com/ee/administration/pages/)
  - [Enabling Elasticsearch](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html)
  - [Setting up Mattermost](https://docs.gitlab.com/ee/integration/mattermost/)
  - [Setting up Prometheus monitoring](https://docs.gitlab.com/ee/administration/monitoring/prometheus/index.html)
  - [Installing / Registering GitLab Runner](https://docs.gitlab.com/runner/install/)
