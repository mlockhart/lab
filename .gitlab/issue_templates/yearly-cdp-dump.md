<!-- 
# Track your work template

(Originally a G-doc: https://docs.google.com/document/d/1TfxQPwNlFVjFFlxBlEMWIYr5z1sGNFp17AGiMnnTffg/edit# )

As many have expressed hesitation in filling out performance review (or promotion) docs until they have an idea of what to add, this doc is meant to be a “dumping ground” of your work, loosely annotated and sorted so that you can easily fill in other docs later.

The most important thing to remember is to keep adding to the doc. Add anything you want. If you’re inclined to make a note of it, then it’s worth adding regardless of the reason. Throw it into whatever category makes sense at the time and worry about whether it’s the “best” category later.

The suggested categories are based on performance and promotion templates so that you can easily fill those out later, but this template is meant to be for guidance only, so feel free to edit to improve it and edit your own copy at will to tailor it to whatever works for you.

This doc also suggests breaking up your tracking to be from December to November as performance reviews are completed in November annually, but you may prefer to use calendar year.
--> 

## Resources to help build/edit this issue:

 * [Performance factor review spreadsheets and template doc](https://drive.google.com/drive/folders/1nj90z5sUCmk9C0WW1zpNwjHeiO3R9XXV) 
 * [Promotion templates](https://drive.google.com/drive/folders/1B22jk4Z5Ps1rrf0uu3J1_AAhPTybXScW)
 * [Support framework and competencies](https://about.gitlab.com/handbook/engineering/career-development/matrix/engineering/support/)


# 2020-12 to 2021-11

## Themes/Highlights

To be completed at the end of the year

## Customer Tickets


## Technical

### Debugging

 * 

### MR to Resolve Issues

 * 

### Contribute to complementary projects
 * 

## Docs

 * 

## Issues/Features Creation/Escalation

 *

## Process/Workflow Improvements

 * 

## Knowledge/Learning/Training completed

 * 

## Mentoring/Training Others (for Senior/Staff)

### Mentor

 * 

### Help hire

 * 

### Help train

 * 

## Values Alignment
[Link to values page](https://about.gitlab.com/handbook/values/)

### Collaboration

 * Slack Example: Thanks for collaborating on difficult upgrade ticket (Slack 2021-01-01)
 *

### Results

 *

### Efficiency

 * 

### Diversity & Inclusion

 * 

### Iteration

 *

### Transparency

 * 

## Miscellaneous


## Slack screenshots

<details>
 <summary>Because screenshots can take up a lot of space, this template suggests adding them at the bottom, but as usual, it’s up to you. A comment thread may also work.</summary>
 <pre>
Example:
2021-01-01: Collaboration
 </pre>
</details>

/confidential
/assign me
