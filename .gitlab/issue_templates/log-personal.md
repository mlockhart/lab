<!-- 
Title:
YYYYMMDD:HH - one-line summary 

Note:- YYYYMMDD is ISO date, and HH is the current UTC hour at start of log 
       unfortunately using /title doesn't work during issue creation -->

<!-- Quick summary to go in the Description on the Issue. The rest is free-form -->

## Intro / one-line summary


---

more words here, free-form

/assign me

/label ~log ~"log::personal"

