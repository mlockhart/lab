<!-- 
Title:
YYYYMMDD:HH - one-line summary 

Note:- YYYYMMDD is ISO date, and HH is the current UTC hour at start of log 
       unfortunately using /title doesn't work during issue creation -->

<!-- these headings are prompts. Remove whatever is not appropriate for this log -->

## Goals

## Hypotheses

What data do I need? How will I get those?

## Setting up build/test/run apparatus

##  Experiment Procedures

##  Outcomes

##  Conclusions

##  Concerns?

Where am I stuck or what don't I understand?

## Next steps

A short plan for how to get unstuck

 - [ ] 

 ---


/assign me

/label ~log ~"log::experiment"
