---
id: 9bot74aiije837z7eoe3eeb
title: Backup
desc: 'Backup VSCode settings'
updated: 1688466817387
created: 1686566962695
---

If this were an EMACS, I would have a source repository to house configuration settings, and a wiki and issues log to discuss them. However VSCode ("viscode") does not require this, if you use the "settings sync" functionality. That is connected with your GitHub or Microsoft username and automatically sync's between all computers.

But, in case GitHub should ever go away, or for some other unforseen emergency, one may need to keep a backup.

## Extensions

- https://code.visualstudio.com/docs/getstarted/settings#_settings-file-locations
- extensions can be saved (this is _all_ extensions, regardless of current VSCode Profile)

  ```shell
  code --list-extensions >> vscode_extensions.txt
  ```

- extensions can be loaded:

  ```shell
  cat vscode_extensions.txt \
    | xargs -n 1 code --install-extension
  ```

## Settings

Global settings are in `<config-dir>/Code/User` with the most important being

- `settings.json`
- `keybindings.json`
