---
id: k5bea29grp63dzud3ogx6fi
title: Clone node
desc: ''
updated: 1694692508904
created: 1694691061085
---

You can create a _similar_ node ("compute instance" or virtual machine) to an existing one using the **CREATE SIMILAR** button on the Google Cloud console's **Compute > [VM instances](https://console.cloud.google.com/compute/instances)** screen. This creates a new node with the same machine settings, but _does not copy/clone the disk_.

***Follow these steps*** to clone a compute node relatively quickly, based on a snapshot of an existing node's disk:

1. In the Google Cloud console, go to **Compute Engine > Storage > [Snapshots](https://console.cloud.google.com/compute/snapshots)**
    - make sure to select the right GCP project in the drop-down at the very top, e.g. `mlockhart-56581c10` 
1. Click **CREATE SNAPSHOT**
1. Enter the **Name** for the snapshot, and select the **Source disk** you will be cloning from the drop-down list of disks for your compute nodes.
1. For cost and speed reasons, unless you _need_ a Multi-regional snapshot, select a _Regional_ **Location** type, and select the same GCP region as the disk you are snapping
1. Click **CREATE**. It will take a minute or so, depending on disk size
1. When it's ready, click on the new snapshot name from the list in the [Snapshots](https://console.cloud.google.com/compute/snapshots) screen
1. We will make a new compute instance from this snapshot. Click on **CREATE INSTANCE** from the snapshot's details screen.
1. Give the new instance a **Name**
1. Select the same **Machine configuration** as the original
1. Make sure it is in the same GCP _region_ as the snapshot
1. If needed, **Allow HTTP** and **HTTPS traffic** through the **Firewall**
1. Click **CREATE**

The new instance will be created and started, and it's root volume will be the snapshot you took of the original VM's disk.
