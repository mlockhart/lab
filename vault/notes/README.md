---
id: 02uqys6pioolalc3qpgnteq
title: README
desc: ''
updated: 1686568354744
created: 1686349046236
---

# Mike's Lab book notes

(notes about these notes)

## What is this vault thing?

This is to become a place for keeping lab notes that supplement the
GitLab Project [Issues](https://gitlab.com/mlockhart/lab/-/issues), (also visible via the [experiments](https://gitlab.com/mlockhart/lab/-/boards/1931655?label_name[]=log%3A%3Aexperiment), [personal log](https://gitlab.com/mlockhart/lab/-/boards/1931647?label_name[]=log%3A%3Apersonal), and [weekly logs](https://gitlab.com/mlockhart/lab/-/issues/?sort=created_date&state=all&label_name%5B%5D=Weekly%20Log%20%F0%9F%93%9D&first_page_size=100)).

The Vault is a collection of [[Markdown|meta.markdown]] hypertext notes which are linked to each other, and also to issues and logs within my lab book. This way I can more easily find issues containing information I want to refer to, and also begin a set of notes which can be kept available offline, and quickly searched.

People at work are using a program called [Obsidian](https://obsidian.md/) to do this. I am experiminting with the open-source [Dendron](https://www.dendron.so/), which is a VSCode plugin that works similarly, but brings the full power of VSCode, and [adds other things](https://wiki.dendron.so/notes/a84ff014-e871-445d-9366-d97f1ad882f1/).

## How I created this vault

I'm still learning about Dendron workspaces, vaults, multi-vaults, self-contained vaults, and how that all fits together. I might have done the Wrong Thing.

1. run ***Dendron: Initialize Workspace***
1. select "**Native workspace**" that will create notes alongside my existing lab book VSCode workspace 
1. select a directory to hold the vault. I went with `vault` instead of the default `docs`, so that it's clear that these markdown files are a Vault

## Dendron Resources

Useful web links to Dendron resources.

- [Getting Started Guide](https://link.dendron.so/6b25)
- [Discord](https://link.dendron.so/6b23)
- [Home Page](https://wiki.dendron.so/)
- [Github](https://link.dendron.so/6b24)
- [Developer Docs](https://docs.dendron.so/)