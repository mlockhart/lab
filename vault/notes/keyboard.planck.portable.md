---
id: vutqiidvl82cu87zoacjvs2
title: Portable
desc: ''
updated: 1689547809824
created: 1687862402227
---
## Typing on the Planck EZ while out of the office

Typing on the Planck is nice, but when out and about, there can be limited space to place the keyboard together with the MacBook. I can disable the built-in keyboard with something like Karabiner Elements, and then place the Planck over the MacBook's keys (so that I can still use the TrackPad), but there's no quick way to change my mind or use the media keys: either it automatically disables when the keyboard is plugged in, so I have to unplug the Planck, or I have to find the option in Karabiner's settings and flip it manually.

Also, when I'm at my desk, I do want both the keyboards to work, and also the ErgoDox (I want _all the keys_)!

I do have an old BlueTooth Magic TrackPad that I could use when out and about...

I think that this arrangement might work out better:

1. connect external magic TrackPad over BlueTooth
1. attach Planck EZ
1. place the TrackPad over the built-in keyboard
1. place the Planck over the built-in TrackPad

Now I won't need to disable the built-in keyboard, because the keys are not being depressed by the Planck sitting on them. The TrackPad can be lightly touched so that it doesn't depress the keys either.

It does mean one extra device to carry around, and also limited battery life for the TrackPad. But I think that the TrackPad can last as long as the MacBook itself on battery. If it does become an issue, then I can purchase a USB-C to Thunderbolt cable for the TrackPad, I suppose.