---
id: dstyu6kvegomq02617zqh5v
title: Auth
desc: ''
updated: 1694987240080
created: 1694986496107
---

When authenticating, GCP library clients [look for details in this order](https://cloud.google.com/docs/authentication/application-default-credentials)

1. `GOOGLE_APPLICATION_CREDENTIALS` [environment variable](https://cloud.google.com/docs/authentication/application-default-credentials#GAC)
1. [User credentials set up by using the Google Cloud CLI](https://cloud.google.com/docs/authentication/application-default-credentials#personal)
1. [The attached service account, returned by the metadata server](https://cloud.google.com/docs/authentication/application-default-credentials#attached-sa)

This is the Application Default Crentials (ADC) strategy. 

You [set up ADC](https://cloud.google.com/docs/authentication/provide-credentials-adc) and then [authorize](https://cloud.google.com/sdk/docs/authorizing#user-account) in one of two ways, depending how you will be using the GCP:

1. [With a User Account](https://cloud.google.com/sdk/docs/authorizing#user-account)
1. [With a Service Account](https://cloud.google.com/sdk/docs/authorizing#service-account)

For GitLab Environment Toolkit (GET), we use `GOOGLE_CREDENTIALS` [environment variable with provisioning service account](# 	see https://gitlab.com/gitlab-org/gitlab-environment-toolkit/-/blob/main/docs/environment_provision.md#configure-authentication-gcp
)