---
id: 0xnzbutxyg4l2dg5icxfkfx
title: Math Markup
desc: ''
updated: 1686697826354
created: 1686607265198
---
For marking up mathematics equations in plain text, use [Katex](https://katex.org/).

Note that GitLab has [a proposal to adopt MathJax instead](https://gitlab.com/gitlab-org/gitlab/-/issues/339109), but for now this [summary from the GitLab documentation](https://docs.gitlab.com/ee/user/markdown.html#math) is a good one:

---

_KaTeX only supports a [subset](https://katex.org/docs/supported.html) of LaTeX._ This syntax also works for the Asciidoctor `:stem: latexmath`. For details, see the [Asciidoctor user manual](https://asciidoctor.org/docs/user-manual/#activating-stem-support).


Math written between dollar signs with backticks (``$`...`$``) or single dollar signs (`$...$`) is rendered inline with the text.

Math written between double dollar signs (`$$...$$`) or in a [code block](https://docs.gitlab.com/ee/user/markdown.html#code-spans-and-blocks) with the language declared as `math` is rendered on a separate line:

````
This math is inline: $`a^2+b^2=c^2`$.

````

>This math is inline: $`a^2+b^2=c^2`$.

````
This math is on a separate line using a `$$...$$` block:

$$
a^2+b^2=c^2
$$
````

>This math is on a separate line using a `$$...$$` block:
>
> $$
> a^2+b^2=c^2
> $$

---

(the `math` fence block is only supported in GitLab, not Dendron)

