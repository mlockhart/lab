---
id: f09zvyjlt0hv6ph5tvsagb8
title: Working on Tasks
desc: Working on tasks in Dendron
updated: 1686621870043
created: 1686602959865
---

The Dendron web has a [handbook](https://handbook.dendron.so/) (inspired by the GitLab handbook ❤️) about how the team operated while it was in Startup mode[^1]. There are some helpful tips on things such as the [weekly journal](https://handbook.dendron.so/notes/CNP6_cMc1K8EtOEFi-e0P/) (and [template](https://handbook.dendron.so/notes/klS6q6LxytW3N-OjLTMOz/)). Other interesting ideas are:

- [Daily Journal](https://handbook.dendron.so/notes/f143773d-3f97-4dbd-8518-603fbb3b0288/) (a bit like [Bullet Journalling](https://bulletjournal.com/))
- [Backlog Journal](https://handbook.dendron.so/notes/McHGENxbFZQ5B335/)
- [Weekly Planning](https://handbook.dendron.so/notes/clMDTci3e_UFbIevRk7nU/)
- [To Dos](https://handbook.dendron.so/notes/0292b34e-47eb-4499-8f49-d9891accdb3d/) (note that the statuses are encoded in the default [[dendron.configuration]])
- [Pruning and Repotting](https://handbook.dendron.so/notes/Rdo1OcupIPYGNmS0NvMRR/)

A lot of this is specific to Dendron's way of doing things as a team, but it is _inspired by GitLab_, and has some overlap. It appears that the team were working from a shared vault, as well as messaging through Discord. It should not matter to me that I don't share my vault with the team in GitLab: I can use issues and GL mentions for this, with a link in the issue to my note, if it helps.

A question presents itself: should I maintain a "daily journal" in this _public_ lab book?  
[[meta.public-vs-private]]

[^1]: Dendron was developed as a going concern and startup for about 2 years, with some funding, and a lot of work was accomplished in that time. Then in February 2023, they [announced on Discord](https://discord.com/channels/717965437182410783/737323300967022732):

    > we were ultimately not able to find product market fit for a venture backed business. After much soul searching, we've made the decision to do a pivot and pursue other business problems.