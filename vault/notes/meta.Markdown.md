---
id: cx3bz95f3x5mybj0b9so22m
title: Markdown
desc: ''
updated: 1686697686673
created: 1686350385910
---

There are various flavours of [Markdown](https://daringfireball.net/projects/markdown/), a [[worse is better|ref.jwz.worse-is-better]] universal information format, that beats the [Org Mode](https://orgmode.org/) and [AsciiDoctor](https://asciidoctor.org/) plain-text markups because it's widely used and implemented, even though those older formats are better specified and more capable.

- [Markdown](https://daringfireball.net/projects/markdown/) is the name of the original plain-text markup [developed in 2004 by John Gruber in collaboration with Aaron Swartz](https://en.wikipedia.org/wiki/Markdown#History)
- [GitHub Flavored Markdown](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax) is a superset of Markdown developed at GitHub, with support for syntax-highlighted fenced code blocks, tables and colour chips. It was used since 2009 to markup README files displayed by the repository viewer, as well as in issues
- [GitLab Flavored Markdown](https://docs.gitlab.com/ee/user/markdown.html) was another superset Markdown, with support for syntax highlighting, tables and other GitLab features in certain contexts, such as READMEs, issues, merge requests, and the wiki
- [CommonMark](https://commonmark.org/) is a standard, unambiguous syntax specification for Markdown formed in 2014. The name was chosen after Gruber objected to the use of _Markdown_ for this standardization effort. CommonMark was adopted by [GitHub](https://github.github.com/gfm/), [GitLab](https://docs.gitlab.com/ee/development/gitlab_flavored_markdown/specification_guide/), [StackOverflow](https://stackoverflow.com/editing-help), and many others, because it has more base features than the original Markdown, it is well specified and tested, and it has an extensions mechanism for site-specific needs
- [Obsidian](https://www.markdownguide.org/tools/obsidian/) is a flavour of CommonMark for taking notes, and supports "wiki links" or "references" of the form `[[link]]`
- [Dendron](https://wiki.dendron.so/notes/ba97866b-889f-4ac6-86e7-bb2d97f6e376/) is similar to Obsidian. Dendron maintains [some compatibility with CommonMark](https://wiki.dendron.so/notes/ba97866b-889f-4ac6-86e7-bb2d97f6e376/#compatibility-with-commonmark)

In addition to markdown, the tools that I use daily support markups for charts and diagrams ([[mermaid]]), mathematics equasions ([[math-markup]]).