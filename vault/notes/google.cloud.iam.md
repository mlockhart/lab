---
id: 3mj9c7203sxk8jy5lw2d8pa
title: IAM
desc: ''
updated: 1694903722892
created: 1694692872137
---

_Identity Access Management_ is a complicated topic, and I think it tripped me up for my GET deployment. Here are notes on what I know:

---

## Managing access to service accounts

- What is a [service account](https://cloud.google.com/iam/docs/service-account-overview)?

- See [Google Cloud Manage access to service accounts](https://cloud.google.com/iam/docs/manage-access-service-accounts)

- Need to make sure to [Enable the IAM API](https://console.cloud.google.com/flows/enableapi?apiid=iam.googleapis.com&redirect=https://console.cloud.google.com)


## Which service account / principal?

I was playing around in [GCP console's IAM Admin](https://console.cloud.google.com/iam-admin/iam?project=mlockhart-56581c10) and I saw to Principals

- `gitlab-get@mlockhart-56581c10.iam.gserviceaccount.com` with roles
    - Compute Admin
    - Kubernetes Engine Admin
    - Service Account Admin
    - Service Account User
- `gitlab-mjl-mac@mlockhart-56581c10.iam.gserviceaccount.com` with roles
    - Compute Admin
    - Compute Network Admin
    - Kubernetes Engine Admin
    - Service Account Admin
    - Service Account User
    - Storage Admin

The `gitlab-mjl-mac@` principal is the one I set up to do deployments using GET on my Mac, with Anton Smith, which I thought was not working, despite having all the roles necessary. The `gitlab-get@` is a principal set up for GET some time ago.

---

For curiosity, I added the missing roles to `gitlab-get@` and then tried a `terraform apply`: it _worked_. Took them away and it failed again.

---

So, that is the principal that is being used, despite my terraform configuration specifying the `gitlab-mjl-mac@` principal in `keys/mlockhart-56581c10-8bc0e30dd679.json` and `ansible/environments/1k_gcp/1k.gcp.yml`.

Well, now that I write this down, the `ansible/environments/...` is not going to be used b Terraform.

---

- There must be a [default account](https://cloud.google.com/docs/authentication/application-default-credentials) that is set somehow to `gitlab-get@`?
- This might also explain why SSH is suddenly not working as it used to?

---

There is a [local account credentials file](https://cloud.google.com/docs/authentication/application-default-credentials#personal) on the workstation

- Linux, macOS: `$HOME/.config/gcloud/application_default_credentials.json`

```json
{
  "client_id": "764086051850-6qr4p6gpi6hn506pt8ejuq83di341hur.apps.googleusercontent.com",
  "client_secret": "d-REDACTED",
  "quota_project_id": "mlockhart-56581c10",
  "refresh_token": "1//REDACTED,
  "type": "authorized_user"
}
```

The `client-id` is different to the one in `keys/mlockhart-56581c10-8bc0e30dd679.json`

```json
...
  "client_id": "104481838115385290889",
...
```

It's also different to any of the listed accounts on the console though.

### I'm Using the environment!

```sh
mjl@modi ~> env|rg GOOG
GOOGLE_CREDENTIALS=/Users/mjl/key/gitlab-get.key.json
GOOGLE_APPLICATION_CREDENTIALS=/Users/mjl/key/gitlab-get.key.json
```

```sh
mjl@modi ~/l/g/m/l/vault (master)> rg client_email $GOOGLE_APPLICATION_CREDENTIALS
6:  "client_email": "gitlab-get@mlockhart-56581c10.iam.gserviceaccount.com",
```

## Configuring to use the right service account(s)

I need to make sure that my dev environment is set to use the service account that has access, and that Terraform and Ansible both use the same account.

I also would like to go back to using my original service account `mlockhart@gitlab.com` for general SSH access on GET-deployed instances, or `mjl` on manually deployed instances.

These are conflicting needs / requirements.

It's probably a good idea to use a dev container to run Terraform and Ansible in, so that it can use its own settings, and then reconfigure the Mac to use the account that I want?

I may need to also [attach the service account](https://cloud.google.com/compute/docs/instances/change-service-account) according to my needs.


```sh
mjl@modi ~ > gcloud compute instances set-service-account alpha \
                 --service-account=mlockhart@gitlab.com \
                 --scopes=default
ERROR: (gcloud.compute.instances.set-service-account) Could not fetch resource:
 - The user does not have access to service account 'mlockhart@gitlab.com'.  User: 'gitlab-mjl-mac@mlockhart-56581c10.iam.gserviceaccount.com'.  Ask a project owner to grant you the iam.serviceAccountUser role on the service account
 ```

Now it thinks it's using `gitlab-mjl-mac@` ?

I think this boils down to an authorization problem:

https://cloud.google.com/sdk/docs/authorizing


```sh
mjl@modi ~ [1]> gcloud auth login
Your browser has been opened to visit:

    REDACTED

You are now logged in as [mlockhart@gitlab.com].
Your current project is [mlockhart-56581c10].  You can change this setting by running:
  $ gcloud config set project PROJECT_ID
mjl@modi ~> gcloud compute instances set-service-account alpha \
                --service-account=mlockhart@gitlab.com \
                --scopes=default
ERROR: (gcloud.compute.instances.set-service-account) Could not fetch resource:
 - The user does not have access to service account 'mlockhart@gitlab.com'.  User: 'mlockhart@gitlab.com'.  Ask a project owner to grant you the iam.serviceAccountUser role on the service account
```

Closer, but it seems this is a user account, not a service account. I might try granting the role?

Didn't help, but, when I use `gcloud compute ssh` it now logs in as the `mlockhart_gitlab_com` service account, instead of `sa_104481838115385290889`

```ssh
mjl@modi ~> gcloud compute ssh alpha
Last login: Sat Sep 16 22:30:21 2023 from 220.239.164.155
openSUSE Leap 15.5 x86_64 (64-bit)

As "root" use the:
- zypper command for package management
- yast command for configuration management

Have a lot of fun...
mlockhart_gitlab_com@alpha:~>
```

(note: to avoid having to specify `--zone`, I set the default region and zone with

```ssh
gcloud config set compute/zone us-west1-c
gcloud config set compute/region us-west1
gcloud config set project "mlockhart-56581c10"
```

)
