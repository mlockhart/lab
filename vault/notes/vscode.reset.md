---
id: l7u1r5ql916ho1vpbzetgnu
title: Reset
desc: 'Reset VSCode'
updated: 1686568418580
created: 1686566183078
---
I once had a problem where the VSCode installation on my work macbook refused to show any editor windows, when opening just one specific folder (my dotfiles). It was not anything to do with groups, or zen mode, or any other settings which I could find. It also persisted even after I removed the `.vscode` directory in this folder.

It must have been something in the `~/Library/Application Support/Code` folder, but I never learnt what it was.

To fix it, I completely reset VSCode on this computer:
 
  1. Quit VSCode
  1. **Nuke** the _entire_ `~/Library/Application Support/Code` folder! 🚨💥
  1. Then restart VSCode, it will be like a vanilla install
  1. Turn on [settings sync](https://code.visualstudio.com/docs/editor/settings-sync)🌐 to get back all my settings and extensions
     - Also need to re-connect GitLab Workflow to gitlab.com and other instances
     - There may be other things that similarly need logging in again?
  1. Open my dotfiles folder and then a file: I can edit the file!

