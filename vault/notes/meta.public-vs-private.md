---
id: 4kphi611kjjfku4bburanen
title: Public Vs Private
desc: ''
updated: 1689547293086
created: 1686605887343
---
My [lab book project on gitlab.com](https://gitlab.com/mlockhart/lab) is a _public_ knowledge base. It is deliberately so, in mind of GitLab's [Transparency Value](https://handbook.gitlab.com/handbook/values/#transparency) and it's an important part of demonstrating my [transparency competancy](https://handbook.gitlab.com/handbook/values/#transparency-competency).

But at the moment, I keep my [weekly log issues](https://gitlab.com/mlockhart/lab/-/issues/?sort=created_date&state=all&label_name%5B%5D=Weekly%20Log%20%F0%9F%93%9D&first_page_size=100) _confidential_.

So, if I keep weekly and daily journals in my notes, should those be in this public vault, or in a separate, private one?

I think the answer for now is: No, **keep the journals _private_**.

- much of my notes can be publicly shared, but I do share the _lab_ externally to GitLab, Inc.
- for anything that truely _is_ public, then public log issues are still the best place to keep those
- I rarely deal with information which needs to be kept [SAFE](https://about.gitlab.com/handbook/legal/safe-framework/)

## Rules for sharing knowledge

1. keep journal note files private in my journal vault
1. keep confidential issues in the lab book project metadata
1. maintain a [single source of truth](https://handbook.gitlab.com/handbook/values/#single-source-of-truth) among these conflicting needs by linking between the vault and other confidential items

I changed my mind: particularly for my _personal notes_, I want one place to go to for searching (the vault), and to be able to link to broader knowledge shared with my peers (GitLab issues/merge requests/handbook pages/tickets/field notes). So that the vault becomes my own SSoT for _everything_. But the lab book is not the best place because it's also externally facing, so it is better to _not_ put my journal in it.  I'm reviving the old [Journal project](https://gitlab.com/mlockhart-private/journal) for that.

It took me a while to decide on this...
