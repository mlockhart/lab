A Docker-Compose for spinning up GitLab + External PostgreSQL

Based upon https://gitlab.com/mlockhart-fork/awesome-compose/-/tree/master/gitlab-postgres  which I explored in https://gitlab.com/mlockhart-fork/awesome-compose/-/merge_requests/1

Change the values for the GitLab and PostgreSQL images, to use different versions.
