#!/bin/bash
# To avoid use of sudo, add user to the docker group:
#
#    sudo usermod -aG docker $USER
#
# But, this script will use it anyway.

source globals.txt
echo Using ${1-env.txt} configuration values
source ${1-env.txt}

PRETTY_VERSION=$(echo $GITLAB_VERSION | sed 's/\./-/g')
CONTAINER_NAME="gitlab-${PRETTY_VERSION}"

if [[ ! -z "$NAME_SUFFIX" ]];
then
  CONTAINER_NAME="${CONTAINER_NAME}-${NAME_SUFFIX}"
fi

CONTAINER_VOLUME_DIR="${GITLAB_HOME}/${CONTAINER_NAME}"

echo "Installing version $GITLAB_VERSION"
echo "Creating directory for version"

sudo mkdir -p "${CONTAINER_VOLUME_DIR}/config/ssl"

echo "Copying SSL certs from '${SSL_CERTS_DIR}'"
sudo cp -v $SSL_CERTS_DIR/* "${CONTAINER_VOLUME_DIR}/config/ssl"

echo "Starting container"
sudo docker run --detach \
  --hostname $EXTERNAL_IP.nip.io \
  --publish $HTTPS_PORT:443 \
  --publish $HTTP_PORT:80 \
  --publish $SSH_PORT:22 \
  --publish $REGISTRY_PORT:5050 \
  --name "${CONTAINER_NAME}" \
  --hostname "${CONTAINER_NAME}" \
  --restart no \
  --volume $CONTAINER_VOLUME_DIR/config:/etc/gitlab \
  --volume $CONTAINER_VOLUME_DIR/logs:/var/log/gitlab \
  --volume $CONTAINER_VOLUME_DIR/data:/var/opt/gitlab \
  --shm-size 512m \
  gitlab/gitlab-ee:$GITLAB_VERSION-ee.0

sudo docker exec ${CONTAINER_NAME} \
  grep Password: /etc/gitlab/initial_root_password
