see https://docs.gitlab.com/ee/install/docker.html#install-gitlab-using-docker-engine

## Files:

- `gitlab.sh` is directly from the documentation
- `new_container.sh` is based on the same idea but customizable
- `env.txt` a "default" environment settings
- `globals.txt` global settings for running from a Spot VM

Then there are configs for some GitLab versions, such as `gl1593.txt` which is for a GitLab 15.9.3.

Run `new_container.sh`. It will load settings from `globals.txt` and `env.txt`. If you give it an argument, that will be used to load from `globals.txt` and the specified file.  You can override the globals by redefining in the specified file.

