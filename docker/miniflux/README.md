# Miniflux RSS reader in containers



To start this up:

1. Start your Docker daemon (e.g. Rancher Desktop on macOS, or built-in on Linux)
1. Start the database: `docker-compose up -d db`
1. Start the app:  `docker-compose up -d miniflux`
1. View the app at http://localhost:8888
1. Import initial feeds from `feeds.opml`

## Following YouTube channels


Google has made it a bit more difficult to subscribe to the RSS/Atom feeds of YouTube Channels, but you can still get the correct URLs:

`https://www.youtube.com/feeds/videos.xml?channel_id=<BROWSE_ID>`

To find the `<BROWSE_ID>`:

1. Browse to the web page of the YouTube Channel you want to follow:
    ```
    https://www.youtube.com/@Gitlab
    ```
1. View the page source
1. Search for the `browse_id` key. We want it's `value`:
    ```json
    {"key":"browse_id","value":"UCnMGQ8QHMAnVIsI3xJrihhg"}
    ```

## Backup and Restore

Based upon [SO:How can I backup a Docker-container with its data-volumes?/docker-compose.yml](https://stackoverflow.com/a/56432886/776953)

### Backup

1. Stop the database: `docker-compose stop db`
1. Run the backup: `docker-compose run --rm db-backup`

    Or, if you want to specify an alternate target name, do:

    ```
    docker-compose run --rm -e TARGET=mybackup db-backup
    ```

1. Resume the database: `docker-compose up -d db`

### Restore

1. Stop the database: `docker-compose stop db`
1. Run the restore: `docker-compose run --rm db-restore`

    Or, restore from a specific file using:

    ```
    docker-compose run --rm -e SOURCE=mybackup db-restore
    ```
1. Resume the database: `docker-compose up -d db`
