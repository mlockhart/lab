#!/usr/bin/env bash

# Create the smallest/cheapest possible compute node:
# a micro 0.25-2 vCPU / 1GB E2 Spot compute node
# with a modest 10GB Standard root volume (openSUSE)
#
# This is estimate to cost US$2.23/month ($0.00/hour).
# The standard disk is $0.4 per month

# Switching from Spot to Standard would make it $6.51/month or $0.01/hour

image=$(gcloud compute images list|awk '/opensuse.*15-5.*x86-64/{print $1}')

gcloud compute instances create zmux \
    --project=mlockhart-56581c10 \
    --zone=us-west1-c \
    --machine-type=e2-micro \
    --network-interface=network-tier=PREMIUM,stack-type=IPV4_ONLY,subnet=default \
    --metadata=enable-oslogin=true \
    --no-restart-on-failure \
    --maintenance-policy=TERMINATE \
    --provisioning-model=SPOT \
    --instance-termination-action=STOP \
    --service-account=709870326368-compute@developer.gserviceaccount.com \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --create-disk=auto-delete=yes,boot=yes,device-name=zmux,image=projects/opensuse-cloud/global/images/${image},mode=rw,size=10,type=projects/mlockhart-56581c10/zones/us-central1-a/diskTypes/pd-standard \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=goog-ec-src=vm_add-gcloud \
    --reservation-affinity=any
