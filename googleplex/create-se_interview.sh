#!/usr/bin/env bash

# Create a GitLab SE Interview VM for conducting interviews It makes a
# VM based upon the one provisioned through the GitLab Support Sandbox.
# I've done this as a backup to the "offical" process, because my
# Sandbox integration to GCP is broken.

# The machine should be deleted after completing the interview.
# It is estimated to cost $25.97 per month ($0.04 per hour).

# Note:- this only makes the VM. It does not provision it for interviews

gcloud compute instances create interview \
    --project=mlockhart-56581c10 \
    --zone=us-west1-c \
    --description=GCP\ \
instance\ used\ by\ Support\ Team\ for\ technical\ interviews \
    --machine-type=n1-standard-2 \
    --network-interface=network-tier=PREMIUM,stack-type=IPV4_ONLY,subnet=default \
    --metadata=MOUNT_DIR=/var/opt,REMOTE_FS=/dev/disk/by-id/google-storage-disk,block-projectsSH-keys=FALSE \
    --maintenance-policy=MIGRATE \
    --provisioning-model=STANDARD \
    --service-account=709870326368-compute@developer.gserviceaccount.com \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --tags=http-server \
    --create-disk=auto-delete=yes,boot=yes,device-name=interview-disk-0,image=projects/ubuntu-os-cloud/global/images/ubuntu-1804-bionic-v20200430,mode=rw,size=10,type=projects/mlockhart-56581c10/zones/us-west1-c/diskTypes/pd-ssd \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=owner=support-team,project=test-interview-vm-0-env-85541428,user=mlockhart-f2dcd61b,goog-ec-src=vm_add-gcloud \
    --reservation-affinity=any

if ! gcloud compute firewall-rules describe allow-http &>/dev/null; then
    gcloud compute firewall-rules create allow-http \
        --direction=INGRESS \
        --priority=1000 \
        --network=default \
        --action=ALLOW \
        --rules=tcp:80 \
        --source-ranges=0.0.0.0/0 \
        --target-tags=http-server
else
    echo "Firewall rule 'allow-http' already exists."
fi
