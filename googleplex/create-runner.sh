#!/usr/bin/env bash

# Create a modest compute node for GitLab Runner on Ubuntu
gcloud compute addresses create runner-static-ip --region=us-west1
gcloud compute instances create runner \
  --machine-type=e2-medium \
  --image-project=ubuntu-os-cloud \
  --image-family=ubuntu-2204-lts \
  --zone=us-west1-c \
  --project=mlockhart-56581c10 \
  --metadata=^,@^ssh-keys=mlockhart:ecdsa-sha2-nistp256\ \
AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBB846UzOIPxb8r/uVkJHRi9DpUQ6OKpHVe/Mdga8GXS\+pQv/rJADgkJm5XKEALQJs0UvZmq6Zr7zXeSzzGk0EEE=\ google-ssh\ \{\"userName\":\"mlockhart@gitlab.com\",\"expireOn\":\"2023-07-12T21:52:19\+0000\"\}$'\n'mlockhart:ssh-rsa\ AAAAB3NzaC1yc2EAAAADAQABAAABAQCtbNX2IajzcPW/L6NGByA/HPN3K/ma4vov6jla0ALotpeynObjCc0f98mKtJNU7rka5o1UVG3KW7tQy76kWe52lvN9A1ElZYcDnqFX2xcZD93BG3jURkvv74kzWaWLwzFL359PqXzGvJs\+8h6/sye2B2k8B8S4UQKpiaBx3Om02JLkEZN4q/pZZ5IzIj9hB\+zgH4vlCWU/8iJPUX7sWZhEIst8q\+CCkWONvUyEb0CBpfhQB/gtRfrr1vGl//fMCtd55NjVKHGowLcOTDyd3Z\+XT8p8wl0u7R5ATzM9kSc\+fgfSYYml16Ig4kpPulkeVI9vHgjMRJywC4yMSifKc/Hd\ google-ssh\ \{\"userName\":\"mlockhart@gitlab.com\",\"expireOn\":\"2023-07-12T21:52:39\+0000\"\}$'\n'mlockhart:ecdsa-sha2-nistp256\ AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBHBxO3VI/LC2tZpAZrRJS5zJhLTLyJhRPU4A8H1KIxd0uj8ehP\+w4VRSnuwBKWtpLWabBUw5xHMAp/ARdVwIWbs=\ google-ssh\ \{\"userName\":\"mlockhart@gitlab.com\",\"expireOn\":\"2023-07-12T21:55:01\+0000\"\}$'\n'mlockhart:ssh-rsa\ AAAAB3NzaC1yc2EAAAADAQABAAABAQCrjFxxw63ktSjuSnk/IT8\+gDXZQPDPfajLXN/GJh2TpiE74eyONsD0kpuIV5TYRx85etYUAnx9m2CPli3gJnqPyuifFl7RDZyPJCLAb6EfK10yFpzkB73PGub\+Zb09nBHQK9iIdGzLUwFs\+k0p98ZMOp19pn9eni/CbyhGw2b/5VWHlTTYJQesqYtU/Y/zWw/qNYMJmXxJDfJtr5aROKvWRMIoPnTREEuvg6JsV8hlVcqTdGaFVAUXAELFBOZtFpemETyEpO7eVQBWzA4vvVRCP635\+NXONLRps97IZUZb1QDgN73GK51c6iMa\+vBubzH6OQa/I2Sp4r1fiW7ikmaJ\ google-ssh\ \{\"userName\":\"mlockhart@gitlab.com\",\"expireOn\":\"2023-07-12T21:55:18\+0000\"\} \
  --service-account=709870326368-compute@developer.gserviceaccount.com \
  --tags=http-server,https-server \
  --address=runner-static-ip

