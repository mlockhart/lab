#!/usr/bin/env bash

# Create a modest compute node for greenhat/zendesk processing:

# an E2-standard-4 4 vCPU / 16GB E2 Spot compute node
# with a large 250GB Standard root volume (openSUSE)
#
# This is estimated to cost US$39.35/month ($0.05/hour).
# The standard disk is $10.00 per month

image=$(gcloud compute images list|awk '/opensuse.*15-5.*x86-64/{print $1}')

gcloud compute instances create greenhat \
    --project=mlockhart-56581c10 \
    --zone=us-west1-c \
    --machine-type=e2-standard-4 \
    --network-interface=network-tier=PREMIUM,stack-type=IPV4_ONLY,subnet=default \
    --metadata=serial-port-enable=true \
    --no-restart-on-failure \
    --maintenance-policy=TERMINATE \
    --provisioning-model=SPOT \
    --instance-termination-action=STOP \
    --service-account=709870326368-compute@developer.gserviceaccount.com \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --tags=http-server,https-server,lb-health-check \
    --create-disk=auto-delete=no,boot=yes,device-name=greenhat,image=projects/opensuse-cloud/global/images/${image},mode=rw,size=250,type=projects/mlockhart-56581c10/zones/us-west1-c/diskTypes/pd-standard \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=goog-ec-src=vm_add-gcloud,instance-ttl-bot-run=2023-october-7,origin=manual \
    --reservation-affinity=any
