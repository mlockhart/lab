#!/usr/bin/env bash
# Update Zypper with Cloud SDK repo information:

if ! zypper repos | grep google-cloud-sdk; then
  sudo rpm --import https://packages.cloud.google.com/yum/doc/yum-key.gpg
  sudo rpm --import https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg

  sudo zypper addrepo \
    --check --gpgcheck --name 'Google Cloud SDK' \
    https://packages.cloud.google.com/yum/repos/cloud-sdk-el7-x86_64 \
    google-cloud-sdk
fi

sudo zypper refresh
sudo zypper install -y google-cloud-sdk
gcloud --version
