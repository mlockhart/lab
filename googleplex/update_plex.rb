#!/usr/bin/env ruby

# This uses my glplex and ssh-update-hostename ruby scripts to
# 1. query the Googleplex for the IP address of the specified node
# 2. update the SSH configuration containing the node, with the new IP address

# These scripts and the unix utilities awk and grep MUST be in the system PATH

# Check for the correct number of command-line arguments
unless ARGV.length == 1
  puts "Usage: #{ File.basename( __FILE__ ) } <pattern>"
  puts "will update the HostName of the node with Host matching <pattern> in"
  puts "your SSH configuration, with the node's current external IP address."
  exit 2
end

PLEX_NODE = ARGV[0]

# Find the external IP address of running nodes matching PLEX_NODE
IP_ADDRESS =
  `glplex list #{ PLEX_NODE } | awk /^#{ PLEX_NODE }.*RUNNING/'{print $6}'`

unless IP_ADDRESS.lines.count == 1
  puts "'There can be only one!' Use a more specific pattern, make sure it's up"
  puts "found: #{ IP_ADDRESS }"
  exit 2
end

# Find which SSH config file contains an entry for PLEX_NODE
conf_file = `grep -Rl #{ PLEX_NODE } ~/.ssh/conf.d/`.strip
conf_file = File.basename conf_file

if conf_file.lines.count == 1
  system "ssh-update-hostname", PLEX_NODE, IP_ADDRESS, conf_file
else
  puts "Could not find #{ PLEX_NODE } in SSH configuration."
  exit 1
end
