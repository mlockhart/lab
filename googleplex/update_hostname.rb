#!/usr/bin/env ruby

SPOT_CONFIG_FILE='gcp-spot.ssh'

# Check for the correct number of command-line arguments
if ARGV.length < 2 || ARGV.length > 3
  puts "Usage: #{ File.basename( __FILE__ ) } <hostname> <new_ip> [file_name]"
  puts "will update ~/.ssh/conf.d/#{ SPOT_CONFIG_FILE } if no file_name is specified"
  exit 1
end

host = ARGV[0]
new_ip = ARGV[1]
home_dir = ENV['HOME']

# Determine the SSH configuration file path and default file name
file_name = ARGV[2] || SPOT_CONFIG_FILE
config_file_path = File.join( home_dir, '.ssh', 'conf.d', file_name )

begin
    config_content = File.read( config_file_path )
rescue Errno::ENOENT
    puts "Could not open #{ config_file_path }"
    exit 1
end

    # Update the specified  host 's IP address in the content.  This uses a tricky regexp with two capture groups:

# 1. ^(\s*Host #{ host }\s*.*\n\s*HostName\s+)
#
#    This part matches a line in the SSH configuration file that starts with the specified host (#{ host }). It captures
#    everything on that line, and the next line, up to the HostName entry.
#
#     - ^ Asserts the start of the line.
#
#     - (\s*Host #{ host }\s*.*\n\s*HostName\s+): Captures the line starting with the specified  host , including any
#       leading or trailing whitespace, a newline, and any more whitespace on the second line up to the HostName entry.
#       The \s* matches any whitespace (including none), and \n represents a newline character.
#
# 2. ([^\s]+)
#
#    This part of the regexp captures the existing hostname address after the HostName entry. It matches one or more
#    characters that are not whitespace, and is not particular about hosntame content (IP address, or DNS)
#
#
# 3.  "\\1#{ new_ip }":
#
#    This is the replacement string for the gsub! method.
#
#     - \\1: Refers to the first captured group in the regexp
#
#     - #{ new_ip } Inserts the new IP address at the end of the captured group.

if config_content.gsub!( /^(\s*Host #{ host }\s*.*\n\s*HostName\s+)([^\s]+)/, "\\1#{ new_ip }" )
  File.write( config_file_path, config_content )
  puts "Updated #{ host } with new IP address: #{ new_ip } in #{ config_file_path }"
else
  puts "Did not update '#{ host }' in #{ config_file_path }\nPerhaps it's not already defined?"
  exit 1
end
