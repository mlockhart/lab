#!/usr/bin/env bash

# Create a modest compute node for general computing on Linux/x86:

# an E2-standard-2 2 vCPU / 8GB E2 compute node
# with a modest 10GB balanced root volume (openSUSE)
#
# This is estimated to cost US$15.68/month ($0.02/hour).
# The balanced disk is $1.00 per month

image=$(gcloud compute images list|awk '/opensuse.*15-5.*x86-64/{print $1}')

gcloud compute instances create alpha-1 \
    --project=mlockhart-56581c10 \
    --zone=us-west1-c \
    --machine-type=e2-standard-2 \
    --network-interface=network-tier=PREMIUM,stack-type=IPV4_ONLY,subnet=default \
    --no-restart-on-failure \
    --maintenance-policy=TERMINATE \
    --provisioning-model=SPOT \
    --instance-termination-action=STOP \
    --service-account=709870326368-compute@developer.gserviceaccount.com \
    --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
    --tags=http-server,https-server \
    --create-disk=auto-delete=yes,boot=yes,device-name=alpha,image=projects/opensuse-cloud/global/images/${image},mode=rw,size=10,type=projects/mlockhart-56581c10/zones/us-west1-c/diskTypes/pd-balanced \
    --no-shielded-secure-boot \
    --shielded-vtpm \
    --shielded-integrity-monitoring \
    --labels=goog-ec-src=vm_add-gcloud,instance-ttl-bot-run=2023-october-7,origin=manual \
    --reservation-affinity=any
