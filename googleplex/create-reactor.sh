#!/usr/bin/env bash

# Create a fairly low cost, largish, compute node:
# an e2-highcpu-16 16 vCPU / 1GB E2 Spot compute node
# with a moderate 64GB Standard root volume (ubuntu)
#
# This is estimate to cost US$102.18/month ($0.14/hour).
# The standard disk is $2.56 per month

# Switching from Spot to Standard would make it $291.47/month or $0.40/hour

gcloud compute instances create reactor \
  --project=mlockhart-56581c10 \
  --zone=us-west1-b \
  --machine-type=e2-highcpu-16 \
  --network-interface=network-tier=PREMIUM,stack-type=IPV4_ONLY,subnet=default \
  --metadata=^,@^ssh-keys=mlockhart:ecdsa-sha2-nistp256\ AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBF8XjcsON8\+tlS0ScUtXGkhjF76S\+9xFAUVPd2vWoGFUAhL\+jh0C/laNWdPtaBYe0qOC8MezlRcL3WI//HMXqcM=\ google-ssh\ \{\"userName\":\"mlockhart@gitlab.com\",\"expireOn\":\"2024-03-03T07:19:18\+0000\"\}$'\n'mlockhart:ssh-rsa\ AAAAB3NzaC1yc2EAAAADAQABAAABAGyEiPgSP13MiG6/\+dH\+mu/9X6H4Txwrvt3mfU8jR2\+L0zP81AqpWkL2eIVfxNxRzxLG9VLhCa8oZNbq4McxxmflkuAQIxdNXNS55/xiTlwpTRoF3NtwfaxhV4JsH0tSuEjU6SaDD9sG/G0E4zqwKiPq3L0rfoY9SFQ55\+Y7k0lV63RZPIwWW15d/p0JUwIuNqUQnlIsj7\+p9pFiIIuCCDtaAAojhQwgmBgFKPycBzO/rZpFlih7JJElPqi8/uCq3iHK6lABcCJpqctdDA6dk9aDnQS1elMc4zc7K5ue4JE057YtkUQy4KYAyqTyjFjHR1F8LI8hTbLSQ3yt0YyqLi8=\ google-ssh\ \{\"userName\":\"mlockhart@gitlab.com\",\"expireOn\":\"2024-03-03T07:19:38\+0000\"\} \
  --no-restart-on-failure \
  --maintenance-policy=TERMINATE \
  --provisioning-model=SPOT \
  --instance-termination-action=STOP \
  --service-account=709870326368-compute@developer.gserviceaccount.com \
  --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
  --tags=http-server,https-server \
  --create-disk=auto-delete=yes,boot=yes,device-name=omega,image=projects/ubuntu-os-cloud/global/images/ubuntu-2310-mantic-amd64-v20231031,mode=rw,size=64,type=projects/mlockhart-56581c10/zones/us-west1-c/diskTypes/pd-standard \
  --no-shielded-secure-boot \
  --shielded-vtpm \
  --shielded-integrity-monitoring \
  --labels=goog-ec-src=vm_add-gcloud,instance-ttl-bot-run=2023-november-25,origin=manual \
  --reservation-affinity=any
